/*---------------选项卡start------------------------*/
        //合约分类选项
       const treatyTypeList=[{
          value:'1',
          label:'工程类合约'
        },{
          value:'2',
          label:'采购类合约'
        },{
          value:'3',
          label:'服务类合约'
        },{
          value:'4',
          label:'土地类合约'
        },{
          value:'5',
          label:'其他类合约'
        }];
        
        //合约分类选项
        const undertakeTypeList=[{
          value:'1',
          label:'施工总承包'
        },{
          value:'2',
          label:'发包人发包专业合同'
        },{
          value:'3',
          label:'发包人自行发包专业'
        }];
        
        //合约分类选项
        const contractClassList=[{
          value:'1',
          label:'单价合同'
        },{
          value:'2',
          label:'总价合同'
        },{
          value:'3',
          label:'其他合同'
        }];
        
      

        /*---------------选项卡end------------------------*/



export default {
  treatyTypeList,
  undertakeTypeList,
  contractClassList,
}