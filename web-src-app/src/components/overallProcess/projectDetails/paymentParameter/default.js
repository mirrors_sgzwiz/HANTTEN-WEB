/*---------------选项卡start------------------------*/
        //合约分类选项
       const treatyTypeList=[{
          value:'1',
          label:'工程类合约'
        },{
          value:'2',
          label:'采购类合约'
        },{
          value:'3',
          label:'服务类合约'
        },{
          value:'4',
          label:'土地类合约'
        },{
          value:'5',
          label:'其他类合约'
        }];
        
        //合约分类选项
        const undertakeTypeList=[{
          value:'1',
          label:'施工总承包'
        },{
          value:'2',
          label:'发包人发包专业合同'
        },{
          value:'3',
          label:'发包人自行发包专业'
        }];
        
        //合约分类选项
        const contractClassList=[{
          value:'1',
          label:'单价合同'
        },{
          value:'2',
          label:'总价合同'
        },{
          value:'3',
          label:'其他合同'
        }];
        
      
        //期数预设列表
        const periodNumList=[{
          value:'0',
          label:'预付款'
        },{
          value:'1',
          label:'第一期'
        },{
          value:'2',
          label:'第二期'
        },{
          value:'3',
          label:'第三期'
        },{
          value:'4',
          label:'第四期'
        },{
          value:'5',
          label:'第五期'
        },{
          value:'6',
          label:'第六期'
        },{
          value:'7',
          label:'第七期'
        },{
          value:'8',
          label:'第八期'
        },{
          value:'9',
          label:'第九期'
        },{
          value:'10',
          label:'第十期'
        },{
          value:'11',
          label:'第十一期'
        },{
          value:'12',
          label:'第十二期'
        },{
          value:'13',
          label:'第十三期'
        },{
          value:'14',
          label:'第十四期'
        },{
          value:'15',
          label:'第十五期'
        },{
          value:'16',
          label:'第十六期'
        },{
          value:'17',
          label:'第十七期'
        },{
          value:'18',
          label:'第十八期'
        },{
          value:'19',
          label:'第十九期'
        },{
          value:'20',
          label:'第二十期'
        }];

        /*---------------选项卡end------------------------*/



export default {
  treatyTypeList,
  undertakeTypeList,
  contractClassList,
  periodNumList,
}