package com.hd.data.dao;

import com.hd.data.entity.OldUserInfo;
import com.hd.data.entity.OldUserInfoExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OldUserInfoMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_user
     *
     * @mbg.generated
     */
    long countByExample(OldUserInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_user
     *
     * @mbg.generated
     */
    int deleteByExample(OldUserInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_user
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer uid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_user
     *
     * @mbg.generated
     */
    int insert(OldUserInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_user
     *
     * @mbg.generated
     */
    int insertSelective(OldUserInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_user
     *
     * @mbg.generated
     */
    List<OldUserInfo> selectByExample(OldUserInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_user
     *
     * @mbg.generated
     */
    OldUserInfo selectByPrimaryKey(Integer uid);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_user
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") OldUserInfo record, @Param("example") OldUserInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_user
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") OldUserInfo record, @Param("example") OldUserInfoExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_user
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(OldUserInfo record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_user
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(OldUserInfo record);
}