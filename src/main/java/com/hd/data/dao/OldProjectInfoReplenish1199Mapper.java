package com.hd.data.dao;

import com.hd.data.entity.OldProjectInfoReplenish1199;
import com.hd.data.entity.OldProjectInfoReplenish1199Example;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OldProjectInfoReplenish1199Mapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_project_info_replenish_1199
     *
     * @mbg.generated
     */
    long countByExample(OldProjectInfoReplenish1199Example example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_project_info_replenish_1199
     *
     * @mbg.generated
     */
    int deleteByExample(OldProjectInfoReplenish1199Example example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_project_info_replenish_1199
     *
     * @mbg.generated
     */
    int insert(OldProjectInfoReplenish1199 record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_project_info_replenish_1199
     *
     * @mbg.generated
     */
    int insertSelective(OldProjectInfoReplenish1199 record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_project_info_replenish_1199
     *
     * @mbg.generated
     */
    List<OldProjectInfoReplenish1199> selectByExampleWithBLOBs(OldProjectInfoReplenish1199Example example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_project_info_replenish_1199
     *
     * @mbg.generated
     */
    List<OldProjectInfoReplenish1199> selectByExample(OldProjectInfoReplenish1199Example example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_project_info_replenish_1199
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") OldProjectInfoReplenish1199 record, @Param("example") OldProjectInfoReplenish1199Example example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_project_info_replenish_1199
     *
     * @mbg.generated
     */
    int updateByExampleWithBLOBs(@Param("record") OldProjectInfoReplenish1199 record, @Param("example") OldProjectInfoReplenish1199Example example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table old_project_info_replenish_1199
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") OldProjectInfoReplenish1199 record, @Param("example") OldProjectInfoReplenish1199Example example);
}