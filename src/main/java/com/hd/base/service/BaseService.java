package com.hd.base.service;

import java.util.Date;

/**
 * Service基类接口
 * @author JLF
 */
public interface BaseService {

    /**
     * 取得当前时间
     * @return
     */
    Date getCurrentDate();

}
