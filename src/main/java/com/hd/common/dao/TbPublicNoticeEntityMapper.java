package com.hd.common.dao;

import com.hd.common.entity.TbPublicNoticeEntity;
import com.hd.common.entity.TbPublicNoticeEntityExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TbPublicNoticeEntityMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_public_notice
     *
     * @mbg.generated
     */
    long countByExample(TbPublicNoticeEntityExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_public_notice
     *
     * @mbg.generated
     */
    int deleteByExample(TbPublicNoticeEntityExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_public_notice
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String noticeId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_public_notice
     *
     * @mbg.generated
     */
    int insert(TbPublicNoticeEntity record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_public_notice
     *
     * @mbg.generated
     */
    int insertSelective(TbPublicNoticeEntity record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_public_notice
     *
     * @mbg.generated
     */
    List<TbPublicNoticeEntity> selectByExample(TbPublicNoticeEntityExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_public_notice
     *
     * @mbg.generated
     */
    TbPublicNoticeEntity selectByPrimaryKey(String noticeId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_public_notice
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") TbPublicNoticeEntity record, @Param("example") TbPublicNoticeEntityExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_public_notice
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") TbPublicNoticeEntity record, @Param("example") TbPublicNoticeEntityExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_public_notice
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TbPublicNoticeEntity record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_public_notice
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TbPublicNoticeEntity record);
}