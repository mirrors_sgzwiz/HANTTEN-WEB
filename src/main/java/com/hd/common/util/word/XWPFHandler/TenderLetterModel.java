package com.hd.common.util.word.XWPFHandler;

import java.util.List;

/**
 * Created by EDZ on 2019/7/16.
 */
public class TenderLetterModel {

    /**
     * 投标单位
     */
    private String input10;

    /**
     * 中标价
     */
    private String input11;

    /**
     * 其中暂列金额及专业工程分包
     */
    private String input12;

    /**
     * input10
     *
     * @return input10 项目描述（略）
     */
    public String getInput10() {
        return input10;
    }

    /**
     * input10
     *
     * @param input10 项目描述（略）
     */
    public void setInput10(String input10) {
        this.input10 = input10;
    }

    /**
     * input11
     *
     * @return input11 项目描述（略）
     */
    public String getInput11() {
        return input11;
    }

    /**
     * input11
     *
     * @param input11 项目描述（略）
     */
    public void setInput11(String input11) {
        this.input11 = input11;
    }

    /**
     * input12
     *
     * @return input12 项目描述（略）
     */
    public String getInput12() {
        return input12;
    }

    /**
     * input12
     *
     * @param input12 项目描述（略）
     */
    public void setInput12(String input12) {
        this.input12 = input12;
    }
}
