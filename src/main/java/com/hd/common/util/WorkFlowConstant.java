package com.hd.common.util;

/**
 * 工作流常量类
 */
public final class WorkFlowConstant {

    //--------------------------------------Key------------------------------------//
    public static final String KEY_LISTENER_VO = "activitiListenVO";
    public static final String KEY_CONTRACT_VO = "contractVO";
    public static final String KEY_TENDER_VO = "tenderTaskVo";
    public static final String KEY_PROJECT_VO = "projectVO";
    public static final String KEY_TASK_PERIOD_VO = "taskPeriodVo";
    public static final String KEY_SEAL_APPLY_VO = "sealApplyVO";
    public static final String KEY_TASK_REEDIT_VO = "taskReeditVO";
    public static final String KEY_TASK_VO = "taskVO";
    public static final String KEY_CASH_APPLY_VO = "requestMenuVO";
    public static final String KEY_ARCHIVE_VO = "archiveVO";
    public static final String KEY_JOB_ID = "jobId";
    public static final String KEY_REJECT_REASON = "rejectReason";
    public static final String KEY_APPROVAL_STATUS = "approvalStatus";
    public static final String KEY_DELEGATE_USER = "delegateUser";
    public static final String KEY_DEPARTMENT_ID = "departmentId";
    public static final String KEY_PROCESS_DICT_TYPE = "processDictType";
    public static final String KEY_MAIN_LEADER_USER = "mainLeaderUser";
    public static final String KEY_SUBMIT_NODE = "submitNode";
    public static final String KEY_APPLY_USER_ID = "applyUserId";
    public static final String KEY_RESOURCE_FILES = "resourceFiles";
    public static final String KEY_PRE_HANDLER = "preHandler";
    public static final String KEY_HANDLE_TIME = "handleTime";
    //--------------------------------------Key------------------------------------//

}
