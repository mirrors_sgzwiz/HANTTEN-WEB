package com.hd.manager.vo;

import com.hd.base.vo.BaseVO;

public class HTProcessVO extends BaseVO {

    /**
     * 流程部署名称
     */
    private String deployName;


    public String getDeployName() {
        return deployName;
    }

    public void setDeployName(String deployName) {
        this.deployName = deployName;
    }

}
