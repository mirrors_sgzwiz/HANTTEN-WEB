package com.hd.manager.vo;

import com.hd.base.vo.BaseVO;

/**
 * 公司VO
 *
 * @author jwl
 * Created in 2019/7/3 15:31
 */
public class HTCooperationCompanyVO extends BaseVO {

    /**
     * 公司ID
     */
    private String companyId;

    /**
     * 合作公司名称
     */
    private String companyName;

    /**
     * 序号
     */
    private String serialNum;

    /**
     * 地址
     */
    private String companyAddress;

    /**
     * 负责人
     */
    private String chargePeople;

    /**
     * 电话
     */
    private String phoneNum;

    /**
     * companyId
     *
     * @return companyId 项目描述（略）
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * companyId
     *
     * @param companyId 项目描述（略）
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    /**
     * companyName
     *
     * @return companyName 项目描述（略）
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * companyName
     *
     * @param companyName 项目描述（略）
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * serialNum
     *
     * @return serialNum 项目描述（略）
     */
    public String getSerialNum() {
        return serialNum;
    }

    /**
     * serialNum
     *
     * @param serialNum 项目描述（略）
     */
    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    /**
     * companyAddress
     *
     * @return companyAddress 项目描述（略）
     */
    public String getCompanyAddress() {
        return companyAddress;
    }

    /**
     * companyAddress
     *
     * @param companyAddress 项目描述（略）
     */
    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    /**
     * chargePeople
     *
     * @return chargePeople 项目描述（略）
     */
    public String getChargePeople() {
        return chargePeople;
    }

    /**
     * chargePeople
     *
     * @param chargePeople 项目描述（略）
     */
    public void setChargePeople(String chargePeople) {
        this.chargePeople = chargePeople;
    }

    /**
     * phoneNum
     *
     * @return phoneNum 项目描述（略）
     */
    public String getPhoneNum() {
        return phoneNum;
    }

    /**
     * phoneNum
     *
     * @param phoneNum 项目描述（略）
     */
    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }
}
