package com.hd.manager.dao.bean;

import com.hd.base.bean.BaseBean;

/**
 * 公司Bean
 *
 * @author jwl
 * Created in 2019/7/3 15:31
 */
public class HTCompanyBean extends BaseBean {

    /**
     * 公司ID
     */
    private String companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 公司名称缩写
     */
    private String condense;

    /**
     * 开户行
     */
    private String bankName;

    /**
     * 账号
     */
    private String bankAccount;

    /**
     * 邮编
     */
    private String postCode;

    /**
     * 电话（开票用）
     */
    private String phoneNum;

    /**
     * 电话（对外联系用）
     */
    private String phoneForeign;

    /**
     * 传真
     */
    private String faxNum;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 公司地址
     */
    private String companyAddress;

    /**
     * phoneForeign
     *
     * @return phoneForeign 项目描述（略）
     */
    public String getPhoneForeign() {
        return phoneForeign;
    }

    /**
     * phoneForeign
     *
     * @param phoneForeign 项目描述（略）
     */
    public void setPhoneForeign(String phoneForeign) {
        this.phoneForeign = phoneForeign;
    }

    /**
     * companyAddress
     *
     * @return companyAddress 项目描述（略）
     */
    public String getCompanyAddress() {
        return companyAddress;
    }

    /**
     * companyAddress
     *
     * @param companyAddress 项目描述（略）
     */
    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    /**
     * companyId
     *
     * @return companyId 项目描述（略）
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * companyId
     *
     * @param companyId 项目描述（略）
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    /**
     * companyName
     *
     * @return companyName 项目描述（略）
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * companyName
     *
     * @param companyName 项目描述（略）
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * condense
     *
     * @return condense 项目描述（略）
     */
    public String getCondense() {
        return condense;
    }

    /**
     * condense
     *
     * @param condense 项目描述（略）
     */
    public void setCondense(String condense) {
        this.condense = condense;
    }

    /**
     * bankName
     *
     * @return bankName 项目描述（略）
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * bankName
     *
     * @param bankName 项目描述（略）
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * bankAccount
     *
     * @return bankAccount 项目描述（略）
     */
    public String getBankAccount() {
        return bankAccount;
    }

    /**
     * bankAccount
     *
     * @param bankAccount 项目描述（略）
     */
    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    /**
     * postCode
     *
     * @return postCode 项目描述（略）
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * postCode
     *
     * @param postCode 项目描述（略）
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    /**
     * phoneNum
     *
     * @return phoneNum 项目描述（略）
     */
    public String getPhoneNum() {
        return phoneNum;
    }

    /**
     * phoneNum
     *
     * @param phoneNum 项目描述（略）
     */
    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    /**
     * faxNum
     *
     * @return faxNum 项目描述（略）
     */
    public String getFaxNum() {
        return faxNum;
    }

    /**
     * faxNum
     *
     * @param faxNum 项目描述（略）
     */
    public void setFaxNum(String faxNum) {
        this.faxNum = faxNum;
    }

    /**
     * email
     *
     * @return email 项目描述（略）
     */
    public String getEmail() {
        return email;
    }

    /**
     * email
     *
     * @param email 项目描述（略）
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
