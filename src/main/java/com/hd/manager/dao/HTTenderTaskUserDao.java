package com.hd.manager.dao;

import com.hd.base.dao.BaseDao;
import com.hd.manager.dao.bean.HTTenderTaskUserBean;

import java.util.List;

/**
 * 批量添加投标任务和成员关联信息
 */
public interface HTTenderTaskUserDao  extends BaseDao {

}
